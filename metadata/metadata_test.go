package metadata_test

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/retire.js/v2/metadata"
)

func TestReportScanner(t *testing.T) {
	want := issue.ScannerDetails{
		ID:      "retire.js",
		Name:    "Retire.js",
		Version: metadata.ScannerVersion,
		Vendor: issue.Vendor{
			Name: "GitLab",
		},
		URL: "https://retirejs.github.io/retire.js",
	}
	got := metadata.ReportScanner

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
