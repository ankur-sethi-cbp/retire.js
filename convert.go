package main

import (
	"encoding/json"
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/retire.js/v2/metadata"
)

func convert(reader io.Reader, prependPath string) (*issue.Report, error) {
	// decode output
	var results []ResultSet
	err := json.NewDecoder(reader).Decode(&results)
	if err != nil {
		return nil, err
	}

	// The output of retire.js contains absolute paths (though not always),
	// so the absolute path of the root directory is needed
	// in order to convert absolute paths to relative paths.
	root := os.Getenv("ANALYZER_TARGET_DIR")
	if root == "" {
		root = os.Getenv("CI_PROJECT_DIR")
	}

	// This environment variable is set when the analyzer
	// installs the npm dependencies (using npm install or yarn install)
	// in order to scan the node_modules directory.
	_, reportNodeModulesPaths := os.LookupEnv("REPORT_NODE_MODULES_PATHS")

	// convertFile converts a file field reported by retire.js
	// to a .location.file field of a security report.
	var convertFile = func(path string) string {
		if path != "" {
			rel := strings.TrimPrefix(strings.TrimPrefix(path, root), "/")
			if !strings.Contains(rel, "node_modules") || reportNodeModulesPaths {
				return rel
			}
		}
		return filepath.Join(prependPath, "package.json")
	}

	// convert output
	issues := []issue.Issue{}
	for _, set := range results {

		// iterate through results
		for _, r := range set.Results {
			for _, v := range r.Vulnerabilities {
				vuln := issue.DependencyScanningVulnerability{
					issue.Issue{
						Category: metadata.Type,
						Scanner:  metadata.IssueScanner,
						Name:     v.Identifiers.Summary, // no package name
						Severity: issue.ParseSeverityLevel(v.Severity),
						Location: issue.Location{
							File: convertFile(set.File),
							Dependency: &issue.Dependency{
								Package: issue.Package{
									Name: r.Component,
								},
								Version: r.Version,
							},
						},
						Identifiers: v.identifiers(),
						Links:       issue.NewLinks(v.Info...),
					},
				}
				issues = append(issues, vuln.ToIssue())

			}
		}

	}

	report := issue.NewReport()
	report.Vulnerabilities = issues
	return &report, nil
}

// ResultSet contains multiple results found in the same file.
type ResultSet struct {
	File    string // can be empty, can the path of a minified version of jquery
	Results []Result
}

// Result contains multiple vulnerabilities affecting the same component.
type Result struct {
	Detection string   // "filename" or empty
	Component string   // package name
	Version   string   // package version
	Parent    struct { // reverse dependency
		Component string
		Version   string
	}
	Vulnerabilities []Vulnerability
}

// Vulnerability is a vulnerability affecting a component.
type Vulnerability struct {
	Info        []string // contains URLs
	Severity    string   // none, low, medium, high, critical
	Identifiers struct {
		Issue   string   // issue number on GitHub
		Summary string   // vulnerability summary
		CVE     []string // CVE ids like "CVE-2015-2951"
	}
}

func (v Vulnerability) identifiers() []issue.Identifier {
	//  extract CVE ids when available
	if len(v.Identifiers.CVE) > 0 {
		ids := make([]issue.Identifier, len(v.Identifiers.CVE))
		for i, cve := range v.Identifiers.CVE {
			ids[i] = issue.CVEIdentifier(cve)
		}
		return ids
	}

	// extract first URL
	if len(v.Info) == 0 {
		// TODO: skip entry and add it to errors/warnings when this will be available
		return []issue.Identifier{}
	}
	url := v.Info[0]

	// turn first URL into an identifier
	return []issue.Identifier{
		parseIdentifierURL(url),
	}
}
