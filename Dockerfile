FROM golang:1.15-alpine AS build

ENV CGO_ENABLED=0

WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM node:14-alpine3.12

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-2.2.4}

# Version of Python, defaults to Python 3
ARG DS_PYTHON_VERSION
ENV DS_PYTHON_VERSION ${DS_PYTHON_VERSION:-3}

RUN set -ex; \
    # ensure we have the most recent versions of all installed packages
    apk upgrade --update-cache --available; \
    \
    # install git for project with git-sourced dependencies \
    apk add --no-cache git python3; \
    \
    # install for python 3 \
    python3 -m ensurepip --default-pip; \
    pip install --upgrade pip setuptools; \
    \
    # temporary workaround for \
    # https://github.com/nodejs/docker-node/issues/813#issuecomment-407339011 \
    npm config set unsafe-perm true; \
    \
    # install retire.js
    npm install -g retire@$SCANNER_VERSION

COPY --from=build /analyzer /analyzer
ENTRYPOINT []
CMD ["/analyzer", "run"]
